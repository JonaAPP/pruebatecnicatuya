package co.com.pruebatecnica.certificacion.utils;

public class ConstantString {

    public static final String BASE_SERVICE = "http://api.geonames.org";
    public static final String SERVICE_SOURCE = "/timezoneJSON";
    public static final String TOKEN = "IFTHESERVICEREQUIREDATOKEN";

    public static final String FORMATTED = "formatted";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static final String USERNAME = "username";
    public static final String STYLE = "style";

    public static final String ALL_PARAMS = "allParams";
    public static final String NO_USER = "noUser";

}
