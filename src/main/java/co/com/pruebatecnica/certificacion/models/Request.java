package co.com.pruebatecnica.certificacion.models;


import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private Boolean formatted;
    private String latitude;
    private String longitude;
    private String userName;
    private String style;
}
