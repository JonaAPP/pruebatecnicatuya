package co.com.pruebatecnica.certificacion.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/geonames_test.feature",
        glue = "co.com.pruebatecnica.certificacion.stepsdefinitions",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class GeonamesTest {
}
