package co.com.pruebatecnica.certificacion.stepsdefinitions;

import co.com.pruebatecnica.certificacion.questions.MessageValidation;
import co.com.pruebatecnica.certificacion.questions.ValidationCode;
import co.com.pruebatecnica.certificacion.tasks.ExecuteGet;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.hamcrest.Matchers;

import java.util.List;
import java.util.Map;

import static co.com.pruebatecnica.certificacion.utils.ConstantString.BASE_SERVICE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class GeonamesStepDefinition {

    @Before
    public void setUpCase(){
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Tester");
    }

    @Given("The user wants to call the service")
    public void theUserWantsToCallTheService() {
        OnStage.theActorInTheSpotlight().whoCan(CallAnApi.at(BASE_SERVICE)).remember("BASE_SERVICE", BASE_SERVICE);
    }
    @When("The user tries to do a request with all params")
    public void theUserTriesToDoARequestWithAllParams(List<Map<String, String>> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(ExecuteGet.with(data.get(0)));
    }

    @Then("The user should see the code of response is successful")
    public void theUserShouldSeeTheCodeOfResponseIsSuccessful(List<Map<String, String>> code) {
        OnStage.theActorInTheSpotlight().should(seeThat(ValidationCode.match(code.get(0).get("responseCode")), Matchers.is(true))
        );
    }

    @When("The user tries to do a request without user param")
    public void theUserTriesToDoARequestWithoutUserParam(List<Map<String, String>> data) {
        OnStage.theActorInTheSpotlight().attemptsTo(ExecuteGet.with(data.get(0)));
    }
    @Then("The user should see the unauthorized code")
    public void theUserShouldSeeTheUnauthorizedCode(List<Map<String, String>> code) {
        OnStage.theActorInTheSpotlight().should(seeThat(ValidationCode.match(code.get(0).get("responseCode")), Matchers.is(true))
        );
    }
    @Then("Should see the message that validate the user field")
    public void shouldSeeTheMessageThatValidateTheUserField(List<Map<String, String>> message) {
        OnStage.theActorInTheSpotlight().should(seeThat(MessageValidation.verifyMessage(message.get(0).get("message"))));
    }

}
