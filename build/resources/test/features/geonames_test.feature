Feature: As an user, I want to consume the geonames service.

  Background: The user consumes the service
    Given The user wants to call the service

  Scenario Outline: Sending all params
    When The user tries to do a request with all params
      | requestType   | formatted   | lat   | lng   | username   | style   |
      | <requestType> | <formatted> | <lat> | <lng> | <username> | <style> |

    Then The user should see the code of response is successful
      | responseCode   |
      | <responseCode> |

    Examples:
      | requestType | formatted | lat | lng | username       | style | responseCode |
      | allParams   | true      | -10 | 20  | qa_mobile_easy | full  | 200          |
      | allParams   | true      | 15 | 20  | qa_mobile_easy | full  | 200          |
      | allParams   | true      |   10 | 10  | qa_mobile_easy | full  | 200          |
      | allParams   | true      | 19 | -10  | qa_mobile_easy | full  | 200          |

  Scenario Outline: The user doesn't send the user param
    When The user tries to do a request without user param
      | requestType   | formatted   | lat   | lng   | style   |
      | <requestType> | <formatted> | <lat> | <lng> | <style> |
    Then The user should see the unauthorized code
      | responseCode   |
      | <responseCode> |
    And Should see the message that validate the user field
      | message   |
      | <message> |
    Examples:
      | requestType | formatted | lat | lng | style | responseCode | message                                                                                                                              |
      | noUser      | true      | -10 | 20  | full  | 401          | Please add a username to each call in order for geonames to be able to identify the calling application and count the credits usage. |


  Scenario Outline: The user send a wrong user param
    When The user tries to do a request with all params
      | requestType   | formatted   | lat   | lng   | username   | style   |
      | <requestType> | <formatted> | <lat> | <lng> | <username> | <style> |
    Then Should see the message that validate the user field
      | message   |
      | <message> |
    Examples:
      | requestType | formatted | lat | lng | style | username  | message                                                                                                                              |
      | allParams      | true      | -10 | 20  | full  | wrongUser | user does not exist. |

